<?php

use App\Http\Controllers\CountController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/no-1', [CountController::class,'index']);
Route::get('/', [CountController::class,'cekOngkir']);
Route::get('/kota', [CountController::class,'getCity']);
Route::post('/cost', [CountController::class,'getCost']);

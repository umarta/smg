<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CountController extends Controller
{
    public function index()
    {
        return view('welcome', [
            'c' => 0,
            'i' => 100
        ]);
    }

    public function cekOngkir(Request $request)
    {
        $response = Http::withHeaders([
            'key' => env('RAJA_KEY'),
        ])->get(env('RAJA_URL') . 'province');

        $res =  $response->json();

        $object = json_decode(json_encode($res), FALSE);
        $provinsis = $object->rajaongkir->results;
        return view('cek_ongkir', compact('provinsis'));
    }

    public function getCity(Request $request)
    {

        $response = Http::withHeaders([
            'key' => env('RAJA_KEY'),
        ])->get(env('RAJA_URL') . 'city?province=' . $request->province);

        $res =  $response->json();

        $object = json_decode(json_encode($res), FALSE);
        return  $object->rajaongkir->results;
    }


    public function getCost(Request $request)
    {

        $payload = [
            'origin'=> 501,
            'destination'=> $request->destination_city,
            'weight'=> $request->weight,
            'courier'=> 'jne'
        ];
        $response = Http::withHeaders([
            'key' => env('RAJA_KEY'),
        ])->post(env('RAJA_URL') . 'cost',$payload);

        $res =  $response->json();

        $object = json_decode(json_encode($res), FALSE);
        return  $object->rajaongkir->results;
    }


}

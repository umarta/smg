<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Repeat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'repeat:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $i = 100;
        $c = 0;
        for ($x = 1; $x <= $i; $x++) {
            if ($c <= 2) {
                if (!fmod($x, 3)) {
                    $this->info('Pasar 20 ' . $x);
                }

                if (!fmod($x, 5)) {
                    $this->info('Belanja pangan ' . $x);
                }
            } else {
                if (!fmod($x, 3)) {
                    $this->info('Belanja pangan ' . $x);
                }

                if (!fmod($x, 5)) {
                    $this->info('Pasar 20 ' . $x);
                }
            }

            if (!fmod($x, 5) && !fmod($x, 3)) {
                $c++;
                $this->info('Pasar 20 Belanja Pangan ' . $x . ' ' . $c);
            }
            if ($c == 5) {
                $this->warn('stop ' . $c);
                return false;
            }
        }
    }
}

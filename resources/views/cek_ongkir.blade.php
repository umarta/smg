<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Cek Ongkir</title>
</head>

<body>
    <div class="container mt-10">
        <div class="container-fluid">
            <form id="cek">
                @csrf
                <div class="form-group">
                    <label for="exampleInputEmail1">Kota Asal</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                        value="Yogyakarta" placeholder="Enter email" disabled>

                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Provinsi Tujuan</label>
                    <select class="js-data-example-ajax form-control" id="province" style="width: 100%">
                        <option >--Pilih Provinsi Tujuan--</option>

                        @foreach ($provinsis as $provinsi)
                            <option value="{{ $provinsi->province_id }}">{{ $provinsi->province }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Kota Tujuan</label>
                    <select class="form-control" id="destination_city" name="destination_city" style="width: 100%">
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Berat (KG)</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                        value="" placeholder="Masukkan berat" name="weight">

                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <div class="m-10" style="margin-top: 50px"></div>
            <div class="card">
                <div class="card-header">Detail Pengiriman</div>
                <div class="card-body detail">
                    <h3 id="courier"></h3>
                    <div class="card-columns" id="list">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script>
        $('#province').change(function() {
            $.ajax({
                url: "{{ url('kota?province=') }}" + $(this).val(),
                type: "get",
                success: function(res) {
                    console.log(res)
                    $('#destination_city').html('');
                    $("#destination_city").html(
                        "<option  >--pilih kota tujuan--</option>")
                    $.each(res, function(i, item) {
                        $('#destination_city').append($('<option>', {
                            value: item.city_id,
                            text: item.city_name
                        }));
                    });
                }
            })
        })
        $('#cek').submit(function(e) {
            e.preventDefault();
            var formData = $(this).serialize()
            $.ajax({
                url: "{{ url('cost') }}",
                type: "post",
                data: formData,
                success: function(res) {
                    $('#courier').text(res[0].name)
                    $.each(res[0].costs, function(i, item) {
                        $('#list').append(
                            '<div class="card">' +
                            '<div class="card-body">' +
                            '<h5 class="card-title">'+item.service+'</h5>' +
                            '<div class="card-text">'+
                            '<p>'+item.description+'</p>'+
                            '<p> Biaya : Rp. '+item.cost[0].value+'</p>'+
                            '<p> Estimasi Pengiriman : '+item.cost[0].etd+' Hari</p>'+
                            '<p> Note :'+item.cost[0].note+'</p>'+
                            '</div>' +
                            '</div>' +
                            '</div>'
                        );
                    });


                }
            })
        })
    </script>
</body>

</html>
